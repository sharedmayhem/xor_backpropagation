package com.learningsystems.xor;

import org.jblas.DoubleMatrix;

public class IO {
    private final DoubleMatrix inputs;
    private final DoubleMatrix outputs;

    public IO(DoubleMatrix inputs, DoubleMatrix outputs) {

        this.inputs = inputs;
        this.outputs = outputs;
    }

    public DoubleMatrix inputs() {
        return inputs;
    }

    public DoubleMatrix outputs() {
        return outputs;
    }

    static class IOBuilder{
        private DoubleMatrix inputs;
        private DoubleMatrix outputs;

        IOBuilder withInputs(DoubleMatrix inputs){
            this.inputs = inputs;
            return this;
        }

        IOBuilder withOutputs(DoubleMatrix outputs){
            this.outputs = outputs;
            return this;
        }

        IO build(){
            return new IO(inputs, outputs);
        }
    }
}
