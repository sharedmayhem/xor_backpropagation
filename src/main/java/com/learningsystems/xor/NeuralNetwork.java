package com.learningsystems.xor;

import org.jblas.DoubleMatrix;

public class NeuralNetwork {

    private final Double learningRate;
    private final Double desiredError;
    private final Double momentum;
    private ForwardPropagator forwardPropagator;
    private int numberOfHiddenNodes;
    private ActivationFunctions.ActivationFunction activationFunction;
    private IO io;

    private NeuralNetwork(IO io, int numberOfHiddenNodes, Double learningRate, ActivationFunctions.ActivationFunction activationFunction, Double desiredError, Double momentum, ForwardPropagator forwardPropagator) {
        this.io = io;
        this.numberOfHiddenNodes = numberOfHiddenNodes;
        this.learningRate = learningRate;
        this.activationFunction = activationFunction;
        this.desiredError = desiredError;
        this.momentum = momentum;
        this.forwardPropagator = forwardPropagator;
    }

    public static void main(String[] args) {
        NeuralNetwork neuralNetwork = getNeuralNetworkWithBinaryInputs(0);
        run(neuralNetwork, new ReportPrinter("BinaryInputs.csv"));

        neuralNetwork = getNeuralNetworkWithBipolarInputs(0);
        run(neuralNetwork, new ReportPrinter("BiPolarInputs.csv"));

        neuralNetwork = getNeuralNetworkWithBipolarInputs(0.9);
        run(neuralNetwork, new ReportPrinter("BiPolarInputsWithMomentum.csv"));
        neuralNetwork.predictOutput(new DoubleMatrix(new double[][]{{-1, -1}}));
    }

    private void predictOutput(DoubleMatrix inputs) {
        ForwardPropagator forwardPropagator = new ForwardPropagator(
                activationFunction, ForwardPropagator.Weights.loadFromFile(numberOfHiddenNodes, inputs.rows));
        DoubleMatrix doubleMatrix1 = forwardPropagator.predictOutput(inputs);
        System.out.println("doubleMatrix1.toString() = " + doubleMatrix1.toString());
    }

    private static void run(NeuralNetwork neuralNetwork, ReportPrinter reportPrinter) {
        try {
            new BackPropagator(neuralNetwork, reportPrinter).train();
        } finally {
            reportPrinter.close();
        }
    }

    public IO getIO() {
        return io;
    }

    public ForwardPropagator getForwardPropagator() {
        return forwardPropagator;
    }

    Double getLearningRate() {
        return learningRate;
    }

    ActivationFunctions.ActivationFunction activationFunction() {
        return activationFunction;
    }

    Double desiredError() {
        return desiredError;
    }

    Double momentum() {
        return momentum;
    }

    private static NeuralNetwork getNeuralNetworkWithBipolarInputs(double momentum) {
        return new Builder()
                .withIO(new IO.IOBuilder()
                        .withInputs(new DoubleMatrix(new double[][]{{-1, -1}, {1, -1}, {-1, 1}, {1, 1}}))
                        .withOutputs(new DoubleMatrix(new double[][]{{-1}, {1}, {1}, {-1}}))
                        .build())
                .withLearningRate(0.2)
                .withActivationFunction(ActivationFunctions.bipolarSigmoid())
                .withHiddenNodes(4)
                .withMomentum(momentum)
                .withDesiredError(5e-5)
                .build();
    }

    private static NeuralNetwork getNeuralNetworkWithBinaryInputs(double momentum) {
        return new Builder()
                .withIO(new IO.IOBuilder()
                        .withInputs(new DoubleMatrix(new double[][]{{0, 0}, {1, 0}, {0, 1}, {1, 1}}))
                        .withOutputs(new DoubleMatrix(new double[][]{{0}, {1}, {1}, {0}}))
                        .build())
                .withLearningRate(0.2)
                .withActivationFunction(ActivationFunctions.sigmoid())
                .withHiddenNodes(4)
                .withMomentum(momentum)
                .withDesiredError(5e-5)
                .build();
    }

    static class Builder {
        private IO io;
        private Double learningRate;
        private ActivationFunctions.ActivationFunction activationFunction;
        private int numberOfHiddenNodes;
        private Double error;
        private Double momentum;

        Builder withIO(IO io) {
            this.io = io;
            return this;
        }

        Builder withLearningRate(Double learningRate) {
            this.learningRate = learningRate;
            return this;
        }

        Builder withDesiredError(Double error) {
            this.error = error;
            return this;
        }

        Builder withMomentum(Double momentum) {
            this.momentum = momentum;
            return this;
        }

        Builder withHiddenNodes(int numberOfHiddenNodes) {
            this.numberOfHiddenNodes = numberOfHiddenNodes;
            return this;
        }

        Builder withActivationFunction(ActivationFunctions.ActivationFunction activationFunction) {
            this.activationFunction = activationFunction;
            return this;
        }

        NeuralNetwork build() {
            ForwardPropagator forwardPropagator = new ForwardPropagator(
                    numberOfHiddenNodes,
                    activationFunction, io.inputs().getRows());
            return new NeuralNetwork(io, numberOfHiddenNodes, learningRate, activationFunction, error, momentum, forwardPropagator);
        }
    }


}