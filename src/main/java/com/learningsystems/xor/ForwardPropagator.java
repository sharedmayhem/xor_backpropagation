package com.learningsystems.xor;

import org.jblas.DoubleMatrix;

import java.io.IOException;
import java.util.PrimitiveIterator;
import java.util.Random;
import java.util.stream.DoubleStream;

class ForwardPropagator {

    private final ActivationFunctions.ActivationFunction activationFunction;
    private final Weights weights;
    private DoubleMatrix activatedOutputOfHiddenLayer;

    ForwardPropagator(int numberOfHiddenNodes, ActivationFunctions.ActivationFunction activationFunction, int numberOfTrainingSamples) {
        this.activationFunction = activationFunction;
        this.weights = Weights.create(numberOfHiddenNodes, numberOfTrainingSamples);
    }

    ForwardPropagator(ActivationFunctions.ActivationFunction activationFunction, Weights weights) {
        this.activationFunction = activationFunction;
        this.weights = weights;
    }

    Weights weights() {
        return weights;
    }

    DoubleMatrix predictOutput(DoubleMatrix inputs) {
        layer1(inputs);
        return layer2();
    }

    DoubleMatrix getActivatedOutputOfHiddenLayer() {
        return activatedOutputOfHiddenLayer;
    }

    private DoubleMatrix layer2() {
        DoubleMatrix inputsToOutputLayer = weights.weightsToOutputLayer().mmul(activatedOutputOfHiddenLayer).add(weights.biasToOutputLayer());
        return layer2(inputsToOutputLayer);
    }

    private DoubleMatrix layer2(DoubleMatrix inputsToOutputLayer) {
        return activationFunction.activate().apply(inputsToOutputLayer);
    }

    private void layer1(DoubleMatrix inputs) {
        DoubleMatrix inputsToHiddenLayer = weights.weightsToHiddenLayer().mmul(inputs.transpose()).add(weights.biasToHiddenLayer());
        activatedOutputOfHiddenLayer = activationFunction.activate().apply(inputsToHiddenLayer);
    }

    static class Weights {
        static final String WEIGHTS_TO_HIDDEN_LAYER_FILE = "WeightsToHiddenLayer.bin";
        static final String WEIGHTS_TO_OUTPUT_LAYER_FILE = "WeightsToOutputLayer.bin";
        private final DoubleMatrix biasToHiddenLayer;
        private final DoubleMatrix biasToOutputLayer;
        private final DoubleMatrix weightsToOutputLayer;
        private final DoubleMatrix weightsToHiddenLayer;
        private DoubleMatrix oldDeltaWeightsToHiddenLayer;
        private DoubleMatrix oldDeltaWeightsToOutputLayer;

        private Weights(int numberOfHiddenNodes, int inputRows) {
            biasToHiddenLayer = new DoubleMatrix(new double[numberOfHiddenNodes][inputRows]);
            biasToOutputLayer = new DoubleMatrix(new double[1][inputRows]);
            weightsToOutputLayer = initWeights(1, numberOfHiddenNodes);//#OutputNeurons * #HiddenNeurons
            weightsToHiddenLayer = initWeights(numberOfHiddenNodes, 2);//#HiddenNeurons * #InputNeurons
            oldDeltaWeightsToHiddenLayer = DoubleMatrix.zeros(weightsToHiddenLayer.getRows(), weightsToHiddenLayer.getColumns());
            oldDeltaWeightsToOutputLayer = DoubleMatrix.zeros(weightsToOutputLayer.getRows(), weightsToOutputLayer.getColumns());
        }


        private Weights(String markerArgument, int numberOfHiddenNodes, int inputRows) {
            weightsToOutputLayer = new DoubleMatrix();
            weightsToHiddenLayer = new DoubleMatrix();
            biasToHiddenLayer = new DoubleMatrix(new double[numberOfHiddenNodes][inputRows]);
            biasToOutputLayer = new DoubleMatrix(new double[1][inputRows]);
            try {
                weightsToOutputLayer.load(WEIGHTS_TO_OUTPUT_LAYER_FILE);
                weightsToHiddenLayer.load(WEIGHTS_TO_HIDDEN_LAYER_FILE);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }


        }

        static Weights loadFromFile(int numberOfHiddenNodes, int inputRows) {
            return new Weights("loadFromFile", numberOfHiddenNodes, inputRows);
        }

        static Weights create(int numberOfHiddenNodes, int inputRows) {
            return new Weights(numberOfHiddenNodes, inputRows);
        }

        void persist() {
            try {
                weightsToHiddenLayer.save(WEIGHTS_TO_HIDDEN_LAYER_FILE);
                weightsToOutputLayer.save(WEIGHTS_TO_OUTPUT_LAYER_FILE);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        DoubleMatrix weightsToHiddenLayer() {
            return weightsToHiddenLayer;
        }

        DoubleMatrix biasToHiddenLayer() {
            return biasToHiddenLayer;
        }

        DoubleMatrix biasToOutputLayer() {
            return biasToOutputLayer;
        }

        void updateWeightsToHiddenLayer(DoubleMatrix deltaWeightsToHiddenLayer, double learningRate, Double momentum) {
            weightsToHiddenLayer.subi((deltaWeightsToHiddenLayer.mul(learningRate)).add(oldDeltaWeightsToHiddenLayer.mul(momentum)));
            oldDeltaWeightsToHiddenLayer = deltaWeightsToHiddenLayer;
        }

        void updateWeightsToOutputLayer(DoubleMatrix deltaWeightsToOutputLayer, double learningRate, Double momentum) {
            weightsToOutputLayer.subi((deltaWeightsToOutputLayer.mul(learningRate)).add(oldDeltaWeightsToOutputLayer.mul(momentum)));
            oldDeltaWeightsToOutputLayer = deltaWeightsToOutputLayer;
        }

        void updateBiasToHiddenLayer(DoubleMatrix deltaBiasToHiddenLayer, double learningRate) {
            biasToHiddenLayer.subi(deltaBiasToHiddenLayer.mul(learningRate));
        }

        void updateBiasToOutputLayer(DoubleMatrix deltaBiasToOutputLayer, double learningRate) {
            biasToOutputLayer.subi(deltaBiasToOutputLayer.mul(learningRate));
        }

        DoubleMatrix weightsToOutputLayer() {
            return weightsToOutputLayer;
        }

        private DoubleMatrix initWeights(int rows, int columns) {
            Random random = new Random();
            DoubleStream doubleStream = random.doubles(rows * columns, -0.5, +0.5);
            PrimitiveIterator.OfDouble iterator = doubleStream.iterator();
            DoubleMatrix result = new DoubleMatrix(rows, columns);
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    result.put(i, j, iterator.next());
                }
            }
            return result;
        }
    }
}
