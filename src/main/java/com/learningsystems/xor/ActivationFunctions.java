package com.learningsystems.xor;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

import java.util.function.UnaryOperator;

class ActivationFunctions {

    static ActivationFunction bipolarSigmoid() {
        return new ActivationFunction() {
            @Override
            public UnaryOperator<DoubleMatrix> activate() {
                return input -> {
                    DoubleMatrix ones = DoubleMatrix.ones(input.getRows(), input.getColumns());
                    return (ones.sub(MatrixFunctions.exp(input.neg()))).div(ones.add(MatrixFunctions.exp(input.neg())));
                };
            }

            @Override
            public UnaryOperator<DoubleMatrix> derivative() {
                return input -> {
                    DoubleMatrix ones = DoubleMatrix.ones(input.getRows(), input.getColumns());
                    return ones.sub(MatrixFunctions.pow(input,2)).mul(0.5);
                };
            }
        };

    }


    static ActivationFunction sigmoid() {
        return new ActivationFunction() {
            @Override
            public UnaryOperator<DoubleMatrix> activate() {
                return input -> {
                    DoubleMatrix ones = DoubleMatrix.ones(input.getRows(), input.getColumns());
                    return ones.div(ones.add(MatrixFunctions.exp(input.neg())));
                };
            }

            @Override
            public UnaryOperator<DoubleMatrix> derivative() {
                return input -> {
                    DoubleMatrix ones = DoubleMatrix.ones(input.getRows(), input.getColumns());
                    return input.mul(ones.sub(input));
                };
            }
        };

    }

    public interface ActivationFunction {
        UnaryOperator<DoubleMatrix> activate();

        UnaryOperator<DoubleMatrix> derivative();
    }
}
