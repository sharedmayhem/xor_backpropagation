package com.learningsystems.xor;

import org.jblas.DoubleMatrix;

import java.util.function.BiFunction;

class BackPropagator {
    private ForwardPropagator forwardPropagator;
    private Double learningRate;
    private IO io;
    private Double desiredErrorRate;
    private Double momentum;
    private ActivationFunctions.ActivationFunction activationFunction;
    private ReportPrinter reportPrinter;

    public BackPropagator(NeuralNetwork neuralNetwork, ReportPrinter reportPrinter) {
        this.forwardPropagator = neuralNetwork.getForwardPropagator();
        this.learningRate = neuralNetwork.getLearningRate();
        this.io = neuralNetwork.getIO();
        this.desiredErrorRate = neuralNetwork.desiredError();
        this.momentum = neuralNetwork.momentum();
        this.activationFunction = neuralNetwork.activationFunction();
        this.reportPrinter = reportPrinter;
    }

    void train() {
        DoubleMatrix inputs = io.inputs();
        DoubleMatrix transposedOutputs = io.outputs().transpose();
        int m = inputs.getRows();
        int epoch = 0;
        Double error = 1d;
        reportPrinter.csv("Epoch", "Total Error");
        while (error > desiredErrorRate) {
            ++epoch;
            DoubleMatrix finalOutput = forwardPropagator.predictOutput(inputs);
            ForwardPropagator.Weights weights = forwardPropagator.weights();
            error = meanSquaredError().apply(finalOutput, io.outputs());

            //Layer 2
            DoubleMatrix activatedOutputOfHiddenLayer = forwardPropagator.getActivatedOutputOfHiddenLayer();

            DoubleMatrix deltaAtOutputLayer = finalOutput.sub(transposedOutputs);
            DoubleMatrix deltaWeightsToOutputLayer = deltaAtOutputLayer.mmul(activatedOutputOfHiddenLayer.transpose()).div(m);
            DoubleMatrix deltaBiasToOutputLayer = deltaAtOutputLayer.div(m);

            //Layer 1
            DoubleMatrix deltaAtHiddenLayer = weights.weightsToOutputLayer().transpose().mmul(deltaAtOutputLayer).mul(activationFunction.derivative().apply(activatedOutputOfHiddenLayer));
            DoubleMatrix deltaWeightsToHiddenLayer = deltaAtHiddenLayer.mmul(inputs).div(m);
            DoubleMatrix deltaBiasToHiddenLayer = deltaAtHiddenLayer.div(m);

            // Weight updates
            weights.updateWeightsToHiddenLayer(deltaWeightsToHiddenLayer, learningRate, momentum);
            weights.updateBiasToHiddenLayer(deltaBiasToHiddenLayer, learningRate);
            weights.updateWeightsToOutputLayer(deltaWeightsToOutputLayer, learningRate, momentum);
            weights.updateBiasToOutputLayer(deltaBiasToOutputLayer, learningRate);

            reportPrinter.csv(String.valueOf(epoch), String.valueOf(error));
            System.out.printf("Epoch: %d Output = %s Error: %s%n", epoch, finalOutput, error);
        }
        forwardPropagator.weights().persist();

    }


    private BiFunction<DoubleMatrix, DoubleMatrix, Double> meanSquaredError() {
        return (actualOutput, expectedOutput) -> {
            DoubleMatrix pow = org.jblas.MatrixFunctions.pow(actualOutput.sub(expectedOutput), 2).mul(0.5);
            return pow.sum();
        };
    }


}
