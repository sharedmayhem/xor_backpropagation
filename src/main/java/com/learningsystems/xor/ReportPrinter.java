package com.learningsystems.xor;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.StringJoiner;

class ReportPrinter {

    private final BufferedWriter bufferedWriter;

    ReportPrinter(String fileName) {
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(fileName));
            System.out.println("Writing to file: " + fileName);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void csv(String... content) {
        try {
            StringJoiner stringJoiner = new StringJoiner(",");
            Arrays.stream(content).forEach(stringJoiner::add);
            bufferedWriter.write(stringJoiner.toString());
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void close() {
        try {
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
